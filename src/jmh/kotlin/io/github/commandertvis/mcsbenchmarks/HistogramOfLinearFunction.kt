package io.github.commandertvis.mcsbenchmarks

import io.github.commandertvis.mcs.McsInterpreter
import org.openjdk.jmh.annotations.*

@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 10)
@Measurement(iterations = 100)
open class HistogramOfLinearFunction {
    @Benchmark
    fun parallelism1() {
        McsInterpreter(1).histogram(
            expression = "2*x+1",
            nValues = 1000000L,
            xMin = 0.0,
            xMax = 1.0,
            histogramMin = 0.0,
            histogramMax = 1.0,
            nBins = 10
        )
    }


    @Benchmark
    fun parallelism4() {
        McsInterpreter(4).histogram(
            expression = "2*x+1",
            nValues = 1000000L,
            xMin = 0.0,
            xMax = 1.0,
            histogramMin = 0.0,
            histogramMax = 1.0,
            nBins = 10
        )
    }
}
