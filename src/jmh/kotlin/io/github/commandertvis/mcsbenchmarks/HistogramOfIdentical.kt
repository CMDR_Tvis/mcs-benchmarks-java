package io.github.commandertvis.mcsbenchmarks

import io.github.commandertvis.mcs.McsInterpreter
import org.openjdk.jmh.annotations.*

@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 10)
@Measurement(iterations = 100)
open class HistogramOfIdentical {
    @Benchmark
    fun paralellism1() {
        McsInterpreter(1).histogram(
            expression = "x",
            nValues = 3000000L,
            xMin = 0.0,
            xMax = 1.0,
            histogramMin = 0.0,
            histogramMax = 1.0,
            nBins = 10
        )
    }

    @Benchmark
    fun paralellism4() {
        McsInterpreter(4).histogram(
            expression = "x",
            nValues = 3000000L,
            xMin = 0.0,
            xMax = 1.0,
            histogramMin = 0.0,
            histogramMax = 1.0,
            nBins = 10
        )
    }
}
