rootProject.name = "mcs-benchmarks-java"

pluginManagement {
    val jmhVersion: String by settings
    val kotlinVersion: String by settings

    plugins {
        id("me.champeau.gradle.jmh") version jmhVersion
        kotlin("jvm") version kotlinVersion
    }
}
