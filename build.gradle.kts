val kotlinApiVersion: String by project
val kotlinJvmTarget: String by project
val kotlinLanguageVersion: String by project
val kotlinVersion: String by project
val mcsInterpreterJavaVersion: String by project
plugins { id("me.champeau.gradle.jmh"); kotlin("jvm") }
group = "io.github.commandertvis.mcs"

repositories {
    jcenter()
    maven("https://gitlab.com/api/v4/projects/18466669/packages/maven")
}

dependencies {
    implementation("io.github.commandertvis.mcs:mcs-interpreter-java:$mcsInterpreterJavaVersion")
    implementation(kotlin("stdlib-jdk8", kotlinVersion))
}

jmh { fork = 1 }

tasks.compileJmhKotlin.get().kotlinOptions {
    apiVersion = kotlinApiVersion
    jvmTarget = kotlinJvmTarget
    languageVersion = kotlinLanguageVersion
}
